# Utilidades
Este arquivo servirá de índice de ferramentas úteis para nosso dia a dia.

***

# API
* [Mashape](https://www.mashape.com/) - Gerenciador de APIs
* [Banco Central](https://www3.bcb.gov.br/sgspub/JSP/sgsgeral/FachadaWSSGS.wsdl) | [Sistema de consulta](https://www3.bcb.gov.br/sgspub/localizarseries/localizarSeries.do?method=prepararTelaLocalizarSeries) - WebService (SOAP) para consulta de dados diversos do Banco Central.

# Autenticação
* [Passport.JS](http://passportjs.org/) - Ferramenta para facilitar no login utilizando contas de Facebook/Twitter

# Bootstrap
* [Bootstrap](http://getbootstrap.com/) - Framework responsivo
* [Flat UI](http://designmodo.github.io/Flat-UI/)
* [Bootflat](http://bootflat.github.io/) - Tema flat para Bootstrap feito em SASS

# Chat
* [Balloons.IO](http://balloons.io/) - Ferramenta de chat com múltiplas salas, login com Facebook/Twitter

# Coffee Script
* [Croque Monsieur](http://croque-monsieur.adriencadet.com/) - Um boilerplate para projetos CS

# Cores
* [Flat UI Colors](http://flatuicolors.com/) - Paleta de cores que geralmente são utilizadas em Flat UI
* [Color Hail Pixel](http://color.hailpixel.com/) - Ferramenta para pegar uma cor em HEXA com base na matiz e saturação em um eixo X/Y

# Encurtamento
* [YOURLS](http://yourls.org/) - Ferramenta para encurtamente customizado de URL
* [YOURLS Link Creator](http://wordpress.org/plugins/yourls-link-creator/) - Plugin do YOURLS para o WordPress

# Favicon
* [Favicon Cheat Sheet](https://github.com/audreyr/favicon-cheat-sheet)
* [Favico.JS](https://github.com/ejci/favico.js) - Adiciona um label número ao favico para quantificar notificações.

# GIT
* [GITignore.IO](http://www.gitignore.io/) - Ferramentar para auxiliar na criação do arquivo .gitignore

# Ícones
* [Font Awesome](http://fortawesome.github.io/Font-Awesome/) - Font dingbat com vários ícones

# IDE
* [Atom](http://atom.io/) - Editor de texto da equipe do GIThub

# Java Script
* [AddEvent](http://addthisevent.com/) - Script para adição de uma data com calendário
* [Elastislide](http://tympanus.net/Development/Elastislide/index4.html) - Slideshow responsivo
* [Gmaps.JS](http://hpneo.github.io/gmaps/) - Facilitando a utilização da API do Google Maps
* [GRID](http://www.sprymedia.co.uk/article/Grid) - Bookmarklet que adiciona uma grid para verificar se o ritmo está correto.
* [Holder.JS](https://github.com/imsky/holder) - Imagens de placeholder geradas dinamicamante.
* [Isotope](http://isotope.metafizzy.co/) - Plugin jQuery para layouts fluidos
* [Salvattore](http://salvattore.com/) - Plugin jQuery para layouts fluidos²
* [Moment.JS](http://momentjs.com/) - Biblioteca JS para tratamento de datas
* [OfflineJS](http://github.hubspot.com/offline/docs/welcome/) - Plugin JS para identificar o status da conexão com a internet
* [Pines Notify](http://pinesframework.org/pnotify/) - Notificações JS para Bootstrap ou jQuery UI
* [Preloaders](http://preloaders.net/en/circular) - Gerador de gifs de loading
* [Ajaxload](http://ajaxload.info) - Gerador de gifs de loading²
* [Select Box](https://github.com/marcj/jquery-selectBox) - JS para substituir a tag `select` por um tempalte custom
* [Toaster](http://codeseven.github.io/toastr/) - Notificações JS
* [Underscore](http://underscorejs.org/) - Biblioteca de helpers JS

# LESS
* [LESS-Sublime](https://github.com/danro/LESS-sublime) - Adicionar suporte de colorização de código para arquivos .LESS
* [Type Quest](https://github.com/tsanguinette/otLESSboilerplate) - Adiciona suporte às features opentype como Ligaturas, Versalete, etc.

# Markdown
* [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax) - Guia da sintaxe markdown
* [Markdown Editor](https://stackedit.io/) - Editor de markdown

# Share Buttons
* [ShareButton](http://sharebutton.co/) - Botões de compartilhamento do Facebook, Twitter e G+ de forma simples e bonita
* [Sharrre](http://sharrre.com/) - Crie seu próprio botão de compartilhamento

# Tipografia
* [CSS Font Stack](http://cssfontstack.com/) - Chamadas CSS de "font-family" para algumas fontes famosas.
* [Google Web Fonts](https://www.google.com/fonts/) - Repositório de webfonts mantido pelo Google
* [Lost Type](http://losttype.com/) - Repositório de fontes gratuitas
* [Squirrel Webfont Generator](http://www.fontsquirrel.com/tools/webfont-generator) - Gerador de webfonts
* [Type Finder](http://www.type-finder.com/) - Ferramenta que sugere fontes de acordo com a sua necessidade mediante algumas perguntas.

# Wireframe
* [Wireframe & UI Kit](http://theycallmecrowe.com/advice/crowe-wireframekit.html) - Kit para prototipação de interfaces.

# WordPress
* [WordPress Plugin Boilerplate](https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate) - Um esqueleto para iniciar o desenvolvimento de um plugin para WordPress